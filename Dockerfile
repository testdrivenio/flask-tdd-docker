# pull official base image
FROM python:3.7.4-alpine

RUN apk update && \
    apk add --virtual build-deps gcc python-dev musl-dev && \
    apk add postgresql-dev && \
    apk add netcat-openbsd && \
    apk add which

# set environment varibles
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# set working directory
WORKDIR /usr/src/app

# add and install requirements
COPY ./Pipfile .
RUN pip install --upgrade pip pipenv && \
    pipenv install && \
    pipenv install --dev

COPY ./entrypoint.sh .
RUN chmod +x entrypoint.sh

# add app
COPY . .
