#!/bin/sh

echo "Waiting for Postgre-SQL...."

while ! nc -z users-db 5432; do
    sleep 0.1
done

echo "Postgre-SQL started...."

pipenv run python manage.py run -h 0.0.0.0
