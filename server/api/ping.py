from flask import Blueprint
from flask_restful import Api, Resource

bp = Blueprint("ping", __name__)
api = Api(bp)


class Ping(Resource):
    def get(self):
        return {"status": "success", "message": "pong"}


api.add_resource(Ping, "/ping")
