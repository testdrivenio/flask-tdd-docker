from flask import Blueprint, request
from flask_restful import Api, Resource
from sqlalchemy import exc

from server import db
from server.api.users.models import User

bp = Blueprint("users", __name__)
api = Api(bp)


class UsersList(Resource):
    def get(self):
        response = {
            "status": "success",
            "data": {"users": [user.to_json() for user in User.query.all()]},
        }
        return response, 200

    def post(self):
        data = request.get_json()
        if not data:
            return {"status": "fail", "message": "Invalid payload."}, 400
        username = data.get("username")
        email = data.get("email")
        try:
            user = User.query.filter_by(email=email).first()
            if not user:
                db.session.add(User(username=username, email=email))
                db.session.commit()

                return {"status": "success", "message": f"{email} was added!"}, 201
            else:
                return (
                    {"status": "fail", "message": "ISorry. That email already exists."},
                    400,
                )
        except exc.IntegrityError:
            db.session.rollback()
            return {"status": "fail", "message": "Invalid payload."}, 400


class Users(Resource):
    def get(self, user_id):
        try:
            user = User.query.filter_by(_id=int(user_id)).first()
            if not user:
                return {"status": "fail", "message": "User does not exist."}, 404
            else:
                response = {
                    "status": "success",
                    "data": {
                        "_id": user_id,
                        "username": user.username,
                        "email": user.email,
                        "active": user.active,
                    },
                }

                return response, 200
        except ValueError:
            return {"status": "fail", "message": "User does not exist."}, 404

    def put(self, user_id):
        data = request.get_json()
        if not data:
            return {"status": "fail", "message": "Invalid payload."}, 400
        username = data.get("username")
        email = data.get("email")
        if not username or not email:
            return {"status": "fail", "message": "Invalid payload."}, 400
        try:
            user = User.query.filter_by(_id=int(user_id)).first()
            if user:
                user.username = username
                user.email = email
                db.session.commit()
                return {"status": "success", "message": f"{user._id} was updated!"}, 200
            else:
                return {"status": "fail", "message": "User does not exist."}, 404
        except exc.IntegrityError:
            db.session.rollback()
            return {"status": "fail", "message": "Invalid payload."}, 400

    def delete(self, user_id):
        try:
            user = User.query.filter_by(_id=int(user_id)).first()
            if not user:
                return {"status": "fail", "message": "User does not exist."}, 404
            else:
                db.session.delete(user)
                db.session.commit()

                return (
                    {"status": "success", "message": f"{user.email} was removed!"},
                    200,
                )
        except ValueError:
            return {"status": "fail", "message": "User does not exist."}, 404


api.add_resource(UsersList, "/users")
api.add_resource(Users, "/users/<user_id>")
