import os


def test_development_config(test_app):
    test_app.config.from_object("server.config.DevelopmentConfig")
    assert isinstance(test_app.config["SECRET_KEY"], str)
    assert not test_app.config["TESTING"]
    assert test_app.config["SQLALCHEMY_DATABASE_URI"] == os.environ.get("DATABASE_URL")


def test_testing_config(test_app):
    test_app.config.from_object("server.config.TestingConfig")
    assert isinstance(test_app.config["SECRET_KEY"], str)
    assert test_app.config["TESTING"]
    assert not test_app.config["PRESERVE_CONTEXT_ON_EXCEPTION"]
    assert test_app.config["SQLALCHEMY_DATABASE_URI"] == os.environ.get(
        "TEST_DATABASE_URL"
    )


def test_production_config(test_app):
    test_app.config.from_object("server.config.ProductionConfig")
    assert isinstance(test_app.config["SECRET_KEY"], str)
    assert not test_app.config["TESTING"]
    assert test_app.config["SQLALCHEMY_DATABASE_URI"] == os.environ.get("DATABASE_URL")
